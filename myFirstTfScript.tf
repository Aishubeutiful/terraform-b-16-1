provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "web-server" {
  ami = "ami-0022f774911c1d690"
  instance_type = "t3.micro"
  security_groups = ["default"]
  key_name = "aws-nv-cbz"
}